const express = require('express');
const path = require('path');
const port = 8000;


const app = express();
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.urlencoded());
app.use(express.static('assets'));

var contactList = [
    {
        Name : "Prem Kumar",
        Phone : "9087654321"
    },
    {
        Name : "Vikash Kumar",
        Phone : "9876545678"
    } 
]


app.get('/', function(req, res){
    return res.render('home', {
        title : "My Contact List",
        contact_List : contactList
    });
});

app.post('/create-contact', function(req, res){
    contactList.push(req.body);

    return res.redirect('back');
});

app.get('/delete-contact', function(req, res){
    let Phone = req.query.Phone;

    let contactIndex = contactList.findIndex(contact => contact.Phone == Phone);

    if(contactIndex !== -1){
        contactList.splice(contactIndex, 1);
    }

    return res.redirect('back');
});



app.listen(port, function(err){
    if(err){
        console.log("Error is Running on Port:", err)
    }

    console.log("Yup! My Express Server is Running on Port:", port);
});